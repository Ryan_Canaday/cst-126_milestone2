<?php 
/*
 *  Author: Ryan C. Canaday
 *  Date: 9/12/19
 *  File: registerhandler.php
 *  
 *  This is a PHP program that creates an online Registration Form.
 */

//error reporting support
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

//constants
define('HOSTNAME', "localhost");
define('USERNAME', "root");
define('PASSWORD', "root");
define('EMPTY_STRING', "");

//global variable
$dbName = "milestone1";
$tableName = "users";

//failure - no data
if (!isset($_POST['submit'])){
    die("Submission failed, no data");
    
}
//success - got data
else {
    //'trim' removes white space at the leading and trailing edge - no blank data
    $firstName = trim($_POST["firstName"]);
    $lastName = trim($_POST["lastName"]);
    $email = trim($_POST["email"]);
    $userName = trim($_POST["UserName"]);
    $password = trim($_POST["Password"]);
}

//establishing a connection
$dbConnect = mysqli_connect(HOSTNAME, USERNAME, PASSWORD);
//connection unsucessful (troubleshooting errors)
if (!$dbConnect) {
    echo "<p>Connection error: " . mysqli_connect_error() . "</p>";
}
//connection sucessful - build the form
else {
    if (mysqli_select_db($dbConnect, $dbName)) {
        //selecting the database
        echo "<p>Sucessfully selected the " . $dbName . " database.</p>";
        echo "<p>" . $firstName . " " . $lastName . " has been registered" . "</p>";
        echo "table name: $tableName<br>";
        
        //else if last name is empty, return an error
        if ($userName === NULL || $userName === EMPTY_STRING) {
            echo "<p> The username is a <b><em>required</em></b> field and can't be blank</p>";
        }
        //else if last name is empty, return an error
        else if ($password === NULL || $password === EMPTY_STRING) {
            echo "<p> The password is a <b><em>required</em></b> field and can't be blank</p>";
        }
        //else submit the form to the database
        else { 
            $sql = "INSERT INTO $tableName(FirstName, LastName, Email, USERNAME, PASSWORD) VALUES ('$firstName', '$lastName', '$email', '$userName', '$password')";
            //tests if the query suceeds or fails
            if ($result = mysqli_query($dbConnect, $sql)) {
                echo "<p>You are now registered.</p>";
            }
            else{
                echo "<p>Error: ". mysqli_error($dbConnect) . "</p>";
            }
        }  
    }
    //troubleshooting errors
    else{
        echo "<p>Could not select the " . $dbName . " database.</p>";
    }
    //close the database
    echo "<br>Database Closing";
    mysqli_close($dbConnect);
}
?>