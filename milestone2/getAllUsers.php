<?php
/*
 *  Author: Ryan C. Canaday
 *  Date: 9/17/19
 *  File: getAllUsers.php
 *
 *  This is a PHP program that gets all the users and displays it on the screen
 */

//error reporting support
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

//constants
define('HOSTNAME', "localhost");
define('USERNAME', "root");
define('PASSWORD', "root");
define('EMPTY_STRING', "");

//global variable
$dbName = "milestone1";
$tableName = "users";

echo "<h2>Milestone Project</h2>";

//establishing a connection
$dbConnect = mysqli_connect(HOSTNAME, USERNAME, PASSWORD);
//connection unsucessful (troubleshooting errors)
if (!$dbConnect) {
    echo "<p>Connection error: " . mysqli_connect_error() . "</p>";
}
//connection sucessful - build the form
else {
    if (mysqli_select_db($dbConnect, $dbName)) {
        //selecting the database
        echo "<p>Sucessfully selected the " . $dbName . " database.</p>";
        echo "table name: $tableName<br>";
        
        $sql = "SELECT ID, FirstName, LastName FROM $tableName";
            //tests if the query suceeds or fails
            if ($result = mysqli_query($dbConnect, $sql)) {
                echo "<h3>Users</h3>";
                if (mysqli_num_rows($result) > 0 ) {
//                    echo "<p>Number of rows in <strong>$tableName</strong> " . mysqli_num_rows($result) . "</p>";
                      $rowNumber = 0;
                      while ($row =  mysqli_fetch_assoc($result)) {
                          ++$rowNumber;
                          echo "<strong>$rowNumber</strong>. ";
                          foreach ($row as $column) {
                              echo "$column ";
                          }
                          echo "<br>";
                      }
                }
                else {
                    echo "<p>No users are registered</p>"; 
                }
            }
            else{
                echo "<p>Error: ". mysqli_error($dbConnect) . "</p>";
            }
    }
    //troubleshooting errors
    else{
        echo "<p>Could not select the " . $dbName . " database.</p>";
    }
    //close the database
    echo "<br>Database Closing";
    mysqli_close($dbConnect);
}
?>