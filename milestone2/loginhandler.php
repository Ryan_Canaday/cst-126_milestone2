<?php 
/*
 *  Author: Ryan C. Canaday
 *  Date: 9/12/19
 *  File: loginhandler.php
 *  
 *  This is a PHP program that creates an online Login Form.
 */

//error reporting support
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

//constants
define('HOSTNAME', "localhost");
define('PASSWORD', "root");
define('USERNAME', "root");
define('EMPTY_STRING', "");

//global variable
$dbName = "milestone1";
$tableName = "users";

//failure - no data
if (!isset($_POST['submit'])){
    die("Submission failed, no data");
    
}
//success - got data
else {
    //'trim' removes white space at the leading and trailing edge - no blank data
    $userName = trim($_POST["UserName"]);
    $password = trim($_POST["Password"]);
}

//establishing a connection
$dbConnect = mysqli_connect(HOSTNAME, USERNAME, PASSWORD);
//connection unsucessful (troubleshooting errors)
if (!$dbConnect) {
    echo "<p>Connection error: " . mysqli_connect_error() . "</p>";
}
//connection sucessful - build the form
else {
    if (mysqli_select_db($dbConnect, $dbName)) {
        //selecting the database
        echo "<p>Sucessfully selected the " . $dbName . " database.</p>";
        echo "<p>" . $userName . " " . "has been loged in" . "</p>";
        echo "table name: $tableName<br>";
        
        //if username is empty, return an error
        if ($userName === NULL || $userName === EMPTY_STRING) {
            echo "<p> The username is a <b><em>required</em></b> field and can't be blank</p>";
        }
        //else if password is empty, return an error
        else if ($password === NULL || $password === EMPTY_STRING) {
            echo "<p> The password is a <b><em>required</em></b> field and can't be blank</p>";
        }
        //else submit the form to the database
        else { 
            $sql = "SELECT ID, USERNAME, PASSWORD FROM $tableName WHERE USERNAME='" . $userName . "'" . "AND PASSWORD='". $password . "'" ;
            //tests if the query suceeds or fails
            if ($result = mysqli_query($dbConnect, $sql)) {
                $nbrRows = mysqli_num_rows($result);
                if ($nbrRows == 1){
                    echo "<p>Login was sucessful.</p>";
                }
                else if ($nbrRows == 0) {
                    echo "<p>Login failed.</p>";
                }
                else {
                    echo "<p>There are multiple duplicate users.</p>";
                }
            }
            else{
                echo "<p>Error: ". mysqli_error($dbConnect) . "</p>";
            } 
        }  
    }
    //troubleshooting errors
    else{
        echo "<p>Could not select the " . $dbName . " database.</p>";
    }
    //close the database
    echo "<br>Database Closing";
    mysqli_close($dbConnect);
}
?>